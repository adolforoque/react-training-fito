import './App.css';
import 'antd/dist/antd.css';
import './index.css';
import React, { useEffect, useState } from 'react';
import { Button, Input, message, Alert, List, Divider } from 'antd';
import { SearchOutlined, StarOutlined, ClearOutlined } from '@ant-design/icons';
import { Typography } from 'antd';

function App() {
  const { Title } = Typography;
  const [valueInput, setvalueInput] = useState('');
  const [word, setWord] = useState('');
  const [wordMinning, setWordMinning] = useState('');
  const [minning, setMinning] = useState('');
  
  const [lista, setList] = useState([]);
  const [init, setInit] = useState('start');
  
  const [data, setdata] = useState([]);
  
  useEffect(() => {
      fetch('https://api.dictionaryapi.dev/api/v1/entries/es/'+word)
      .then(response => response.json())
      .then(data => data.title && data.title == 'No Definitions Found' ? message.error('No se encuentra esa palabra', 2) :
            data[0].meaning["nombre masculino"] ? setWordMinning(data[0].meaning["nombre masculino"][0].definition) : 
            data[0].meaning["nombre femenino"] ? setWordMinning(data[0].meaning["nombre femenino"][0].definition) :
            data[0].meaning["nombre masculino y femenino"] ? setWordMinning(data[0].meaning["nombre masculino y femenino"][0].definition) :
            data[0].meaning["pronombre demostrativo"] ? setWordMinning(data[0].meaning["pronombre demostrativo"][0].definition) : message.error('No se encuentra esa palabra', 2)
           );
  }, [word]);
  
  useEffect(() => {
      var l = lista.concat([minning]);
      if (minning != '') { setList(l.sort()); localStorage.setItem(minning, minning); }
  }, [minning]);
  
  useEffect(() => {
      var arrayOfValues = [];
      for (var i in localStorage){
          if (localStorage.hasOwnProperty(i)){
              arrayOfValues.push(localStorage[i]);
          }
      }
      setList(arrayOfValues.sort());
  }, [init]);
  
  useEffect(() => {
      setdata(lista);
  }, [lista]);

  function handleRemove(id) {
      var l = lista.filter((item) => item !== id);
      setList(l.sort());
      localStorage.removeItem(id);
  }
  
  function removeAll() {
      var l = [];
      setList(l);
      localStorage.clear();
      
  }

  return (
    <div className="App">
        <Divider orientation="left"><Title level={4}>Significado de las Palabras</Title></Divider>
        <Input placeholder="Escriba una Palabra" onChange={(event) => setvalueInput(event.target.value)} value={valueInput} />
        
        <Button type="primary" icon={<SearchOutlined />} onClick={() => setWord(valueInput)}> Buscar</Button>
        <Button type="primary" icon={<StarOutlined />}onClick={() => setMinning(wordMinning != '' ? word + ": " + wordMinning : '')}> Agragar a Favoritos</Button>
        <Divider orientation="left">Definición</Divider>
        <p>{ wordMinning }</p>

        <br/> 
       <Divider orientation="left"><Title level={4}>Lista de Favoritos:</Title></Divider>
        <List 
          itemLayout="horizontal"
          size="small"
          bordered
          dataSource={data}
          renderItem={item => (
          
          <List.Item>
               <Button type="primary" icon={<ClearOutlined />} onClick={() => handleRemove(item)}>Eliminar</Button>{item}
          </List.Item>
          
          )}
        />
        
        <Button type="primary" icon={<ClearOutlined />} onClick={() => removeAll()}>Vaciar Lista</Button>
        
    </div>
  );
}
export default App;
